import React, { Component } from 'react';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
//Componente de vista de home
import Home from './components/Home';
//Componente de vista de detalle
import Detalle from './components/Detalle';

class App extends Component {

  render() {
    return (
      <Router>
        <div>
          <Switch>
              <Route exact path='/' component={Home} />
              <Route path='/detalle/:uniqueID' component={Detalle} />
          </Switch>
        </div>
      </Router>
    );
  }
}

export default App;
