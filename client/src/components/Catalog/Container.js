import React, { Component } from 'react';
//Componente de item del catalogo
import Item from './Item';
//Estilos del catalogo
import '../../static/css/catalog.css';

/*
Clase container para el catalogo de productos
*/
class Container extends Component {

  state = {products: []}

  componentDidMount() {
    fetch('/api')
      .then(res => res.json())
      .then(products =>
        this.setState({ products }));
  }

  render() {
    return (
      <div>
        <div className="container">
          <div className="banner-img">
            <img src="https://minisitios.ripley.cl/minisitios/icdinamica/computacion20190221/img/iccomputacion-mb.jpg" alt="computación" className="show-xs" />
          </div>
          <div className="banner-img">
            <img src="https://minisitios.ripley.cl/minisitios/icdinamica/computacion20190221/img/iccomputacion.jpg" alt="computación" className="show-lg" />
          </div>
        </div>
        <div className="container">
          <div className="resultados-totales">
            <p>{this.state.products.length} Resultados de productos</p>
          </div>
          <div className="catalog-grid">
          {this.state.products.map(product =>
            <Item key={product.uniqueID} info={product}/>
          )}
          </div>
        </div>
      </div>
    );
  }
}

export default Container;
