import React, { Component } from 'react';
import { Link } from 'react-router-dom';

/*
Clase para el renderizado de los productos en el catalogo
*/
class Item extends Component {

  render() {
    return (
      <div className="item" >
        <Link to={'detalle/'+this.props.info.uniqueID}>
  				<div className="image-container">
  					<img
  						src={'https:'+this.props.info.fullImage}
  						alt="notebook" />
  				</div>
  				<div className="product-name">{this.props.info.name}</div>
          {this.calculateListPrice()}
          {this.calculateCardPrice()}
        </Link>
			</div>
    );
  }

  /*
  Función para obtener el listado de precios de un producto
  */
  calculateListPrice() {

    if(this.props.info.prices.discountPercentage > 0){
      return  <div>
                <div className="old-price">{this.props.info.prices.formattedListPrice}</div>
                <div className="offer-price">{this.props.info.prices.formattedOfferPrice}</div>
                <div className="discount-badget">-{this.props.info.prices.discountPercentage}%</div>
              </div>

    }else{
      return <div>
              <div className="offer-price">{this.props.info.prices.formattedListPrice}</div>
            </div>
    }
  }

  /*
  Función para obtener el precio por tarjeta Ripley en caso que aplique
  */
  calculateCardPrice() {

    if(this.props.info.prices.cardPrice != null){
     return  <div className="ripley-price">
                  <div>{this.props.info.prices.formattedCardPrice}</div>
                  <img className="catalog-prices__card-price-image"
                    src="https://static.ripley.cl/images/opex.png"
                    alt="Precio Tarjeta Ripley" />
                </div>

    }
  }
}

export default Item;
