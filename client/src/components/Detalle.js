import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import Header from './Header/Container';
import Description from './Detalle/Description';
//Estilos base
import './../static/css/base.css';
//Estilos del detallo
import './../static/css/detalle.css';

/*
Component para renderizar la vista del detalle
*/
class Detalle extends Component {

  state = {product: []}

  /*
  Se obtiene la información del producto segun el ID en la URL
  */
  componentDidMount() {

    const { match: { params } } = this.props;

    fetch(`/api/product/?id=${params.uniqueID}`)
      .then(res => res.json())
      .then(product =>
        this.setState({ product }));
  }

  render() {
    return (
      <div className="ripley-detail">
        <Header />
        <div className="container">

          <div className="detalle-info">
            <Link className="go-back" to={''}>Volver al catálogo</Link>
            <div className="product-name">
              {this.state.product.name}
            </div>
            <div className="product-sku">
              SKU: {this.state.product.partNumber}
            </div>
            <div className="product-image">
              <img src={this.state.product.fullImage} alt="product" />
            </div>
            <div className="product-prices">
            {this.calculateListPrice()}
            </div>
          </div>
          <div class="detalle-info">
            <Description product={this.state.product}/>
          </div>
        </div>
      </div>
    );

  }

  /*
  Función para obtener el listado de precios de un producto
  */
  calculateListPrice() {

    if(this.state.product.hasOwnProperty("prices")){

      if(this.state.product.prices.discountPercentage > 0){
        return   <div>
                  <div className="price-row old-price">
                    <div className="price-name">Normal</div>
                    <div className="price-value">{this.state.product.prices.formattedListPrice}</div>
                  </div>
                  <div className="price-row offer-price">
                    <div className="price-name">Internet</div>
                    <div className="price-value">{this.state.product.prices.formattedOfferPrice}</div>
                  </div>
                  {this.calculateCardPrice()}
                  <div className="price-row discount">
                    <div className="price-name">Descuento</div>
                    <div className="price-value">-{this.state.product.prices.discountPercentage}%</div>
                  </div>
                </div>

      }else{
        return  <div className="price-row old-price">
                  <div className="price-name">Normal:</div>
                  <div className="price-value">{this.state.product.prices.formattedListPrice}</div>
                </div>
      }
    }

  }

  /*
  Función para obtener el precio por tarjeta Ripley en caso que aplique
  */
  calculateCardPrice() {

    if(this.state.product.prices.cardPrice != null){
      return  <div className="price-row ripley-price">
                <div className="price-name">Tarjeta Ripley</div>
                <div className="price-value">
                  <img className="catalog-prices__card-price-image"
                  src="https://static.ripley.cl/images/opex.png"
                  alt="Precio Tarjeta Ripley" />
                  {this.state.product.prices.formattedCardPrice}
                </div>
              </div>
    }

  }


}

export default Detalle;
