import React, { Component } from 'react';

/*
Component de descripción del producto
*/
class Description extends Component {

  state = {
    description: true,
    specifications: false
  }

  constructor(){
    super();
    this.openContent = this.openContent.bind(this);
  }

  render() {
    return (
      <div className="accordion">
        <div className={this.openClass('description')}>
          <div onClick={() => this.openContent('description')} className="tab">
            <p className="open-label">Descripción</p>
            <div className="view-more">{this.openText('description')}</div>
          </div>
          <div className="content">
            <div dangerouslySetInnerHTML={{ __html: this.props.product.longDescription }}></div>
          </div>
        </div>
        <div className={this.openClass('specifications')}>
          <div onClick={() => this.openContent('specifications')} className="tab">
            <p className="open-label">Especificaciones</p>
            <div className="view-more">{this.openText('specifications')}</div>
          </div>
          <div className="content">
            {this.renderSpecs()}
          </div>
        </div>
      </div>
    );

  }

  /*
    Clase de abierto o cerrado
  */
  openClass(itemName) {
    let classItem = "item ";
    classItem += this.state[itemName] ? 'open' : 'close';
    return classItem;
  }

  /*
    Abrir contenido
  */
  openContent(itemName) {
     this.setState({ [itemName] : !this.state[itemName] });
  }

  /*
    Texto de abierto o cerrado
  */
  openText(itemName) {
    return this.state[itemName] ? 'Ver menos' : 'Ver más';
  }

  /*
    Función para renderizar un listado de especifaciones
  */
  renderSpecs(){

    if(this.props.product.attributes){
      return <ul>{this.props.product.attributes.map(spec => this.listSpecs(spec) )}</ul>;
    }

  }

  /*
    Función para renderizar y validar un item de especificación
  */
  listSpecs(spec){

    if(spec.displayable === true && spec.usage === 'descriptive'){
      return <li key={spec.id}><div>{spec.name}</div><div>{spec.value}</div></li>
    }else{
      return null;
    }
  }

}

export default Description;
