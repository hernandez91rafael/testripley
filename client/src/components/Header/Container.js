import React, { Component } from 'react';
//Componente de cintillo de detalle
import Ribbon from './Ribbon';
//Estilos generales del header
import '../../static/css/header.css';

/*
Clase container para el header
*/
class Container extends Component {

  render() {
    return (
      <div>
        <Ribbon />
        <div className="navigation-header">
      		<header className="ripley-header" data-reactroot="">
      			<section className="ripley-header__main-header">
      				<nav className="header-nav">
      					<a href="/" className="logo"><img src={require('../../static/images/logo.png')} alt="logo"/></a>
      				</nav>
      			</section>
      			<div id="algolia-search-suggestions-container"></div>
      		</header>
      	</div>
      </div>
    );
  }
}

export default Container;
