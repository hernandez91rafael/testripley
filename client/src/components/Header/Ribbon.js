import React, { Component } from 'react';
//Estilos del cintillo de despacho
import '../../static/css/ribbon.css';

/*
Componente del cintillo de despacho
*/
class Ribbon extends Component {

  render() {
    return (
      <div className="marketing-ribbon">
    		<div className="cintillodespacho19">
    			<a
    				href="https://simple.ripley.cl/tecno/compra-hoy-recibe-hoy?ic-sup-cintillo-recibehoy">
    				<picture>
    				<img
    					className="show-xs" src="https://minisitios.ripley.cl/minisitios/home/campanas/despacho/img/bg-mb.svg"
    					alt="" />
            <img
      					className="show-lg" src="https://minisitios.ripley.cl/minisitios/home/campanas/despacho/img/bg.svg"
      					alt="" />
            </picture>
    			</a>
    		</div>
    		<div className="container-flex base_navbar_corporativo_header">
    			<div className="container">
    				<nav id="menu_corporativo_header">
    					<ul id="navbar_corporativo_header">
    						<li className="nav-item"><a className="nav-link" target="_blank" rel="noopener noreferrer"
    							href="https://simple.ripley.cl?menu_corporativo">Ripley.com</a></li>
    						<li className="nav-item"><a className="nav-link" target="_blank" rel="noopener noreferrer"
    							href="https://www.bancoripley.cl?menu_corporativo">Banco
    								Ripley</a></li>
    						<li className="nav-item"><a className="nav-link" target="_blank" rel="noopener noreferrer"
    							href="https://simple.ripley.cl/minisitios/ripleypuntos?menu_corporativo">Ripley
    								Puntos Go</a></li>
    						<li className="nav-item"><a className="nav-link" target="_blank" rel="noopener noreferrer"
    							href="http://www.segurosripley.cl/SeguroAutomotriz?menu_corporativo">Seguros
    								Ripley</a></li>
    						<li className="nav-item"><a className="nav-link" target="_blank" rel="noopener noreferrer"
    							href="https://simple.ripley.cl/minisitios/clubes">Clubes
    								Ripley</a></li>
    					</ul>
    				</nav>
    			</div>
    		</div>
    	</div>
    );
  }
}

export default Ribbon;
