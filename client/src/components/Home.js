import React, { Component } from 'react';
//Componente del header
import Header from './Header/Container';
//Componente del container del catalogo
import Catalog from './Catalog/Container';
import './../static/css/base.css';

/*
Component para renderizar la vista del catalogo
*/
class Home extends Component {
  render() {
    return (
      <div className="ripley-catalogo">
        <Header />
        <Catalog />
      </div>
    );
  }
}

export default Home;
