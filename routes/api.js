var express = require('express');
var router = express.Router();
var request = require('request');
var redis = require('redis');

//Se obtiene los productos a mostrar
var products = require('./../public/products.json');

//Se crea el cliente de Redis
const client = redis.createClient('redis://h:p001f30a4e4e25d9591ab5d7d810d96b77d779da5e1ef555bd39bd016e865bced@ec2-3-223-240-236.compute-1.amazonaws.com:10989');

//Se imprimen errores de Redis en la consola
client.on('error', (err) => {
    console.log("Error " + err)
});

//Obtener un producto por su ID
router.get('/product', function(req, res, next) {

    //Se genera un rediskey por cada ID de producto
    var redisKey = 'product-id-'+req.query.id;

    //Se obtiene el Key de Redis
    return client.get(redisKey, (err, cachedApi) => {

      //Si existe el registro se devuelve el JSON desde Redis
      if (cachedApi) {
          return res.json(JSON.parse(cachedApi));
      } else {

        //URL de la API de productos
        var requestUrl = 'https://simple.ripley.cl/api/v2/products/by-id/' + req.query.id;

        request(requestUrl, function (error, response, body) {

          if (!error && response.statusCode == 200) {

            res.json(JSON.parse(body));

            //Se actualiza el valor del key de Redis por un minuto
            client.setex(redisKey, 60, body);

          }else{
            res.json('error');
          }

        });

      }
   });
});

//Obtener un listado de productos
router.get('/', function(req, res, next) {

  var redisKey = 'ripley-products';

  // Try fetching the result from Redis first in case we have it cached
    return client.get(redisKey, (err, cachedApi) => {

      //Si existe el registro se devuelve el JSON desde Redis
      if (cachedApi) {
          return res.json(JSON.parse(cachedApi));
      } else {

        //PartNumbers de los productos
        var productParam = '?partNumbers='+products.join();
        var requestUrl = 'https://simple.ripley.cl/api/v2/products' + productParam;

        request(requestUrl, function (error, response, body) {

          if (!error && response.statusCode == 200) {

            res.json(JSON.parse(body));
            //Se actualiza el key de Redis
            client.setex(redisKey, 60, body);
          }
        });
      }
      
   });
});

module.exports = router;
